package controllers

import (
	"encoding/json"

	md "gitee.com/countpoison/youbei/models"
	jobs "gitee.com/countpoison/youbei/utils/jobs"
)

func (c *MainController) JobTest() {
	ob := new(md.Task)
	c.APIReturn(500, "解析失败", json.Unmarshal(c.Ctx.Input.RequestBody, ob))
	zipfile, err := jobs.TestBackup(ob)
	c.APIReturn(500, "备份失败", err)
	rs := []md.RemoteStorage{}
	if len(ob.RS) > 0 {
		c.APIReturn(500, "查询远程存储失败", md.Localdb().In("id", ob.RS).Find(&rs))
	}
	for _, v := range rs {
		c.APIReturn(500, v.Name+"存储失败", jobs.TestRemote(zipfile, v))
	}
	c.APIReturn(200, "测试成功", nil)
}

func (c *MainController) RunJob() {
	c.APIReturn(500, "失败", jobs.Backup(c.Ctx.Input.Param(":id"), true))
	c.APIReturn(200, "执行成功", nil)
}
